//
// implementation of markov chains for text generation.
// sorajsm
//

#define SIZE(a) (sizeof(a)/sizeof(*a))

#include <bits/stdc++.h>
#include <stdint.h>
using namespace std;
int main() {
	srand(time(NULL));
	//
	// markov_memory: # of preceeding words that form a chain.
	// markov_map[chain][word] # of times this particular chain has preceeded the word.
	// markov_count[chain] # of times a particular chain has appeared.
	//
	int markov_memory = 4;
	map<string, map<string, int> > markov_map;
	map<string, int> markov_count;

	vector<string> preceed_seed; preceed_seed.reserve(markov_memory);
	{
		//
		// keep track of the N preceeding words. note that [0] holds the oldest word.
		//
		vector<string> preceed; preceed.reserve(markov_memory);
		string line;
		while(getline(cin, line)) {
			//
			// include whitelisted punctuation. e.g. It's        -> It's
			// delimit other special characters e.g. hello/world -> hello world
			// transform to lowercase           e.g. OBVIOUS     -> obvious
			//
			static const char whitelist[] = {
				',', '.', '-', ';', ':', '?', '!', '\''
			};
			string clean; clean.reserve(line.size());
			for(size_t ln_i = 0; ln_i < line.size(); ++ln_i) {
				char ch = line[ln_i];
				if(isalpha(ch)) {
					clean.push_back(tolower(ch));
					continue;
				} 
				//
				// check if this character is whitelisted.
				// if it is not, then replace it with whitespace.
				//
				size_t wl_i;
				for(wl_i = 0; wl_i < SIZE(whitelist); ++wl_i) {
					if(ch != whitelist[wl_i]) continue;
					break;
				}
				if(wl_i < SIZE(whitelist)) {
					clean.push_back(ch);
				} else if(clean.size() && clean[clean.size() - 1] != ' ') {
					clean.push_back(' ');
				}
			}
			//
			// trick to avoid handling the special case: "word\0" in the loop below.
			//
			clean.push_back(' ');
			//
			// process each word of the cleaned string.
			// keep track of the N preceeding words. note that [0] holds the oldest word.
			//
			size_t wstart = (size_t)(-1);
			for(size_t clean_i = 0; clean_i < clean.size(); ++clean_i) {
				if(isalpha(clean[clean_i]) && wstart == (size_t)(-1)) {
					wstart = clean_i;
				} else if(isspace(clean[clean_i]) && wstart != (size_t)(-1)) {
					string word = clean.substr(wstart, clean_i - wstart);
					wstart = (size_t)(-1);

					//
					// check if we have enough preceeding words to form a chain.
					//
					if((int)preceed.size() < markov_memory) {
						preceed_seed.push_back(word);
						preceed.push_back(word);
						continue;
					}

					string chain;
					if(markov_memory > 0) {
						chain = preceed[0];
						for(size_t pre_i = 1; pre_i < preceed.size(); ++pre_i) {
							chain.append(preceed[pre_i]);
							//
							// shift preceeding words to make room for the most recently parsed word.
							//
							preceed[pre_i - 1] = preceed[pre_i];
						}
						preceed[preceed.size() - 1] = word;
					}

					//
					// register this chain-word relationship.
					//
					++markov_map[chain][word];
					++markov_count[chain];
				}
			}
		}
	}

	if((int)preceed_seed.size() < markov_memory) {
		fprintf(stderr, "not enough text available to fill a memory of %d words.\n", markov_memory);
		return 0;
	}

	//
	// how many words to output.
	//
	int query_n_words = 1000;

	vector<string> preceed = preceed_seed;
	for(int word_i = 0; word_i < query_n_words; ++word_i) {
		string chain = preceed[0];
		for(size_t pre_i = 1; pre_i < preceed.size(); ++pre_i) {
			chain.append(preceed[pre_i]);
			//
			// shift preceeding words to make room for the most recently generated word.
			//
			preceed[pre_i - 1] = preceed[pre_i];
		}

		//
		// reset if we've found a chain which does not have any children.
		//
		map<string, int>::iterator mc_it = markov_count.find(chain);
		if(mc_it == markov_count.end()) {
			preceed = preceed_seed;
			continue;
		}

		//
		// select a random word which has previously followed from this chain; weight using # of occurences of such a transition.
		//
		int val = rand()%mc_it->second;
		map<string, int>::iterator mm_it = markov_map[chain].begin();
		while(true) {
			val -= mm_it->second;
			if(val > 0) ++mm_it;
			else break;
		}
		preceed[preceed.size() - 1] = mm_it->first;

		//
		// for more readable output, separate text data into sentences when it is logical to do so.
		//
		static const char sentence_delimeters[] = {
			'.', '!', '?'
		};
		printf("%s", mm_it->first.c_str());
		size_t sd_i;
		for(sd_i = 0; sd_i < SIZE(sentence_delimeters); ++sd_i) {
			if(mm_it->first[mm_it->first.size() - 1] != sentence_delimeters[sd_i]) continue;
			break;
		}
		if(sd_i < SIZE(sentence_delimeters)) printf("\n");
		else printf(" ");
	}
	printf("\nGenerated %d words.\n", query_n_words);

	return 0;
}
