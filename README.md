# Tiny Markov Chain #

Tiny implementation of markov chains for text generation. Public domain.


## Trained on the King James Bible ##
```
therefore the fathers shall eat the sons in the midst of the land among the people.
and the priests answered and said, it is a light thing for him to walk in the law 
of moses, all this evil is come upon us: consider, and behold our reproach.
sickness there be; what prayer and supplication soever be made by any man, or by 
all thy people israel, when every one shall know his own sore and his own grief, 
and shall spread forth his hands in the midst of her; and they shall become a prey 
and a spoil to all their enemies; because they have done that which was evil in my 
sight, and have provoked me to anger with their vanities.
```

## Trained on Shakespeare ##
```
heaven is here, where juliet lives; 
and every cat and dog and little mouse, 
every unworthy thing, live here in heaven and earth, 
call not me honour' nor lordship.' i ne'er drank sack 
in my life; and happy were i in england now, as once 
i was, and held for certain the king will know him one day.
```